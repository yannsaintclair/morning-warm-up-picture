app();

function app(){
  var d = new Date();
  //set categories for each day of the week
  var dayoftheweek = [{day: "Sunday", category: "sport"},{day: "Monday", category: "nature"},{day: "Tuesday", category: "portrait"},{day: "Wednesday", category: "street"},{day: "Thursday", category: "animals"},{day: "Friday", category: "food-drink"},{day: "Saturday", category: "travel"}];
  var tag = dayoftheweek[d.getDay()];

  //write date
  writeDate(tag.day,d);

  //return Json with Jqury
  $.getJSON('https://api.unsplash.com/search/photos?client_id=c2b7e49cae088f1b1d07e113444ec6058f892aec1e72fd4930da08d9289fb8dc&orientation=landscape&per_page=1&query='+tag.category, function(data){
    var portfolioUrl = data.results[0].user.portfolio_url;
    //display photo
    document.getElementById('image').src = data.results[0].urls.regular;
    //write credit
    if (portfolioUrl===null) {
      document.getElementById('credit').innerHTML = "Photography by "+data.results[0].user.first_name+" "+data.results[0].user.last_name;
    } else {
      document.getElementById('credit').innerHTML = "Photography by <a href='"+portfolioUrl+"'>"+data.results[0].user.first_name+" "+data.results[0].user.last_name+"</a>";
    }
  });
};

function writeDate(notd,date){
  var abb;
  var dotm = date.getDate();
  if (dotm == 1 || dotm == 31) {
    abb = "st";
  } else if (dotm == 2) {
    abb = "nd";
  } else if (dotm == 3) {
    abb = "rd"
  } else {
    abb = "th"
  }
  document.getElementById('date').innerHTML = notd+" the "+dotm+abb;
};



$("img").on("mousemove", function(event) {
  $("#titleHover").css({"display":"block","left":event.pageX+20,"top":event.pageY-30});
})
       .mouseleave(function(){
          $("#titleHover").css("display","none");
        });
;
