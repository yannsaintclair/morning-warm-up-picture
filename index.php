<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="lightbox.js"></script>
    <link rel="stylesheet" href="style.css">
    <title></title>
  </head>
  <body>
    <div id="container">
      <?php include 'mnwp.php'; app();?>
      <p id="titleHover">[every morning a new picture </br>for your morning sketching routine]</p>

      <h1>Morning Warm-up Pic</h1>
      <p id="date"><?php echo date("l") . " the " . date("dS"); ?></p>

      <img onclick="lightbox()" src="<?php echo $image; ?>" alt="" id="image">

      <footer><?php echo $credit; ?></footer>

      <div id="modal" onclick="lightbox()">

      </div>
    </div>
  </body>
  <script type="text/javascript">
    $("img").on("mousemove", function(event) {$("#titleHover").css({"display":"block","left":event.pageX+20,"top":event.pageY-30});})
            .mouseleave(function(){$("#titleHover").css("display","none");});
  </script>
</html>
