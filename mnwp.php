<?php
function app(){


     //read local Json
     $localJSON = file_get_contents("./picture.json", true);
     $data = json_decode($localJSON);
     $portfolioUrl =  $data->user->portfolio_url;

     //store image
     global $image;
     $image = $data->urls->regular;

     //store image credit
     if ($portfolioUrl===null) {
       global $credit;
       $credit = "Photography by ". $data->user->first_name . " " . $data->user->last_name;
     } else {
       global $credit;
       $credit = "Photography by <a href='".$portfolioUrl."'>".$data->user->first_name . " " . $data->user->last_name."</a>";
     }
 }
?>
